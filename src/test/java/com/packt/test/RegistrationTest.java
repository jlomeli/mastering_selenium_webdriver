package com.packt.test;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.packt.base.BaseTest;

public class RegistrationTest extends BaseTest{
	
	@Test
	public void registrationTest() {
		driver.get("http://demo.automationtesting.in/Register.html");
		
		WebElement firstName = driver.findElement(By.xpath("//input[@placeholder='First Name']"));
		WebElement lastName = driver.findElement(By.xpath("//input[@placeholder='Last Name']"));
		WebElement phone = driver.findElement(By.xpath("//input[@type='tel']"));
		
		WebElement address = driver.findElement(By.xpath("//textarea[@class='form-control ng-pristine ng-untouched ng-valid']"));
		WebElement email = driver.findElement(By.xpath("//input[@type='email']"));
		WebElement genderCheckbox = driver.findElement(By.xpath("//input[@value='Male']"));
		 
		WebElement countryDropdown = driver.findElement(By.xpath("//select[@id='countries']"));
		Select countrySelection = new Select(countryDropdown);
		WebElement yearDropdown = driver.findElement(By.id("yearbox"));
		Select yearSelection = new Select(yearDropdown);
		WebElement monthDropdown = driver.findElement(By.xpath("//select[@placeholder='Month']"));
		Select monthSelection = new Select(monthDropdown);
		WebElement dayDropdown = driver.findElement(By.id("daybox"));
		Select daySelection = new Select(dayDropdown);
		
		WebElement password = driver.findElement(By.id("firstpassword"));
		WebElement passwordConfirmation = driver.findElement(By.id("secondpassword"));
		WebElement submitButton = driver.findElement(By.id("submitbtn"));
		
		firstName.sendKeys("Juan");
		lastName.sendKeys("Doe");
		phone.sendKeys(getPhone(10));
		
		address.sendKeys("123 fake st.");
		email.sendKeys("juandoe" + getTime() + "@test.com");
		genderCheckbox.click();
		
		countrySelection.selectByValue("United States");
		yearSelection.selectByValue("1990");
		monthSelection.selectByValue("June");
		daySelection.selectByValue("10");
		
		password.sendKeys("Admin123");
		passwordConfirmation.sendKeys("Admin123");
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		submitButton.click();
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	private String getTime() {
		SimpleDateFormat formatter = new SimpleDateFormat("MMddyyyyHHmmss");
		Date date = new Date();
		return formatter.format(date);
	}
	
	private String getPhone(int count) {
		String NUMERIC_STRING = "0123456789";
		StringBuilder builder = new StringBuilder();
		
		while (count-- != 0) {
			int character = (int)(Math.random()*NUMERIC_STRING.length());
			builder.append(NUMERIC_STRING.charAt(character));
		}
		
		return builder.toString();
	}
}
