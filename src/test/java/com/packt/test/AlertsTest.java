package com.packt.test;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.packt.base.BaseTest;

public class AlertsTest extends BaseTest{
	
	@Test(priority = 1)
	public void jSAlertButtonTest() {
		driver.get("http://the-internet.herokuapp.com/javascript_alerts");
		System.out.println("Page opened!");
		
		//Click alert button
		WebElement jSAlertButton = driver.findElement(By.xpath("//button[@onclick='jsAlert()']"));
		jSAlertButton.click();
		
		WebDriverWait wait = new WebDriverWait(driver, 3);
		wait.until(ExpectedConditions.alertIsPresent());
		
		Alert alert = driver.switchTo().alert();
		String alertText = alert.getText();
		System.out.println(alertText);
		
		Assert.assertTrue(alertText.equals("I am a JS Alert"), "Alert text is not the expected");
		
		sleep();
		
		alert.accept();
		
		WebDriverWait resultWait = new WebDriverWait(driver, 3);
		Assert.assertTrue(resultWait.until(ExpectedConditions.textToBePresentInElementLocated(By.id("result"), "You successfuly clicked an alert")), "Result text is not the expected");
		
		sleep();
	}
	
	@Test(priority = 2)
	public void jSAlertConfirmTest() {
		driver.get("http://the-internet.herokuapp.com/javascript_alerts");
		System.out.println("Page opened!");
		
		WebElement jSAlertNButton = driver.findElement(By.xpath("//button[@onclick='jsConfirm()']"));
		jSAlertNButton.click();
		
		WebDriverWait alertWait = new WebDriverWait(driver, 3);
		alertWait.until(ExpectedConditions.alertIsPresent());
		
		Alert alert = driver.switchTo().alert();
		String alertText = alert.getText();
		System.out.println(alertText);
		
		Assert.assertTrue(alertText.equals("I am a JS Confirm"), "Alert text is not the expected");
		
		sleep();
		
		alert.accept();
		
		WebDriverWait resultWait = new WebDriverWait(driver, 3);
		Assert.assertTrue(resultWait.until(ExpectedConditions.textToBePresentInElementLocated(By.id("result"), "You clicked: Ok")), "Result text is not the expected");
		
		sleep();
	}
	
	@Test(priority = 3)
	public void jSAlertCancelTest() {
		driver.get("http://the-internet.herokuapp.com/javascript_alerts");
		System.out.println("Page opened!");
		
		WebElement jSAlertButton = driver.findElement(By.xpath("//button[@onclick='jsConfirm()']"));
		jSAlertButton.click();
		
		WebDriverWait alertWait = new WebDriverWait(driver, 3);
		alertWait.until(ExpectedConditions.alertIsPresent());
		
		Alert alert = driver.switchTo().alert();
		String alertText = alert.getText();
		System.out.println(alertText);
		
		Assert.assertTrue(alertText.equals("I am a JS Confirm"), "Alert text is not the expected");
		
		sleep();
		
		alert.dismiss();
		
		WebDriverWait resultWait = new WebDriverWait(driver, 3);
		Assert.assertTrue(resultWait.until(ExpectedConditions.textToBePresentInElementLocated(By.id("result"), "You clicked: Cancel")), "Result text is not the expected");
		
		sleep();
		
	}
	
	@Test(priority = 4)
	public void jSAlertPromptTest() {
		driver.get("http://the-internet.herokuapp.com/javascript_alerts");
		System.out.println("Page opened!");
		
		WebElement jSAlertButton = driver.findElement(By.xpath("//button[@onclick='jsPrompt()']"));
		jSAlertButton.click();
		
		WebDriverWait alertWait = new WebDriverWait(driver, 3);
		alertWait.until(ExpectedConditions.alertIsPresent());
		
		Alert alert = driver.switchTo().alert();
		String alertText = alert.getText();
		System.out.println(alertText);
		
		Assert.assertTrue(alertText.equals("I am a JS prompt"), "Alert text is not the expected");
		
		sleep();
		
		alert.sendKeys("Hello World!");
		alert.accept();
		
		WebDriverWait resultWait = new WebDriverWait(driver, 3);
		Assert.assertTrue(resultWait.until(ExpectedConditions.textToBePresentInElementLocated(By.id("result"), "You entered: Hello World!")), "Result text is not the expected");
		
		sleep();
	}
}
