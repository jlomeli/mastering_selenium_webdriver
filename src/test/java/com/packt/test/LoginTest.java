package com.packt.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.packt.base.BaseTest;

public class LoginTest extends BaseTest{
	
	@Test
	public void logInTest() {
		
		driver.get("http://the-internet.herokuapp.com/login");
		System.out.println("Page opened");
		
		WebElement username = driver.findElement(By.id("username"));
		WebElement password = driver.findElement(By.name("password"));
		WebElement logInButton = driver.findElement(By.tagName("button"));
		
		password.sendKeys("SuperSecretPassword!");
		username.sendKeys("tomsmith");
		
		//takeScreenshot("filled_fields");
		
		logInButton.click();
		
		takeScreenshot("click_executed");
		
		WebElement messageElement = driver.findElement(By.id("flash-messages"));
		String message = messageElement.getText();
		
		Assert.assertTrue(message.contains("You logged into a secure area!"),"Message doesn't contain expected text.");
	}
}
